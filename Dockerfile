FROM python:3-alpine

RUN apk add jq && \
    python3 -m pip install -U awscli && \
    adduser -Ds /bin/sh aws

USER aws